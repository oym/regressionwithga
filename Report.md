# 教師あり学習レポート

## 1. コードの説明

### 1.1 概要

遺伝的アルゴリズムを用いて、回帰分析モデルの決定係数が最大/最小となる特徴量の組み合わせを特定する。  
さらに、最大となる特徴量の組み合わせ・最小となる特徴量の組み合わせ・全特徴量のそれぞれで、  
学習データ・検証データの分け方を変えながら、回帰分析モデルを30回生成し、その際の決定係数の平均・最大・最小を出力する。

### 1.2 使い方

```
python regression_ga [入力ファイル] [世代数]
```

### 1.3 動作環境

python3.7.3

### 1.4 使用したライブラリ

- [Deap](https://github.com/deap/deap)

### 1.5 class GA_Selector

遺伝的アルゴリズムを用いて、回帰分析モデルの決定係数が最大/最小となる
特徴量の組み合わせを特定するクラス。

#### 1.5.1 メンバ関数

- \_\_init\_\_：コンストラクタ
- evalIndividual：個体評価のための関数
- select：遺伝的アルゴリズムによる特徴量選択

#### 1.5.2 メンバ変数

- MGEN：世代数
- MU：個体の選択数
- LAMBDA：1世代の個体数
- CXPB：交叉確率
- MUTPB：突然変異確率
- X：特徴量のデータ
- y：目的変数のデータ
- test_size：データから、検証データを取り出す割合

### 1.6 ユーティリティ関数

- get_used_column_names_list：Deapでは特徴量ごとに"使用する(1)/しない(0)"が
                              返ってくるので、それを特徴量名のリストに変換する関数
- read_column_name_list：ファイルから特徴量名のリストを取得する関数
- standardization：学習データと検証データの標準化を行う関数
- learn_and_eval：回帰分析モデルの生成と決定係数の算出を行う関数

## 2. 結果

### 2.1 [実験1]特徴量：ストレスデータ・世代数：100

```
---------------
[all]
all feature set score(avg):0.10811717862542033
all feature set score(max):0.1751153429595056
all feature set score(min):0.04581758545406667

[best]
best feature set:eyeMoveDown,eyeMoveRight,accY,accZ
best feature set score(avg):0.11252407603548886
best feature set score(max):0.16761392434807443
best feature set score(min):0.048851543403868776

[worst]
worst feature set:yaw
worst feature set score(avg):-0.004788265589590183
worst feature set score(max):0.0015247541273979426
worst feature set score(min):-0.016635729513350572
---------------
```

### 2.3 [実験3]特徴量：ストレスデータとそれらの2つの特徴量の乗算(2条項)・世代数：100

```
---------------
[all]
all feature set score(avg):-1.0907936217883696
all feature set score(max):0.12671840836151094
all feature set score(min):-12.037555184249578

[best]
best feature set:accY,accZ,eyeMoveUp*eyeMoveUp,eyeMoveUp*eyeMoveDown,
                 eyeMoveDown*accZ,eyeMoveRight*accZ,blinkSpeed*yaw,
                 blinkStrength*pitch,yaw*accY,accZ*accZ
best feature set score(avg):0.11720352906461849
best feature set score(max):0.16350924344725482
best feature set score(min):0.02452129129361247

[worst]
worst feature set:pitch*accX
worst feature set score(avg):-0.0032637930368905536
worst feature set score(max):0.003102772046549185
worst feature set score(min):-0.03830791550494328
---------------
```

### 2.5 感想

GAによる特徴量選択で、決定係数の平均値が多少、向上している。
一方、決定係数の最大値は、全部使った方が最も高いケースがある。
これは、今回の課題では、特徴量の選択よりも、学習データと検証データの切り分け方の影響が大きいということな気がする。

2乗項を特徴量として追加したケースでは、最大の決定係数は0.12で最小の決定係数は-12となっている。
このように、最大と最小に大きな差がある場合は、データの切り分け方の影響が極めて大きいことを意味すると思うので、
決定係数の値が大きくても、信頼性の低いモデルだと考える。

## 3. 参考文献

- [遺伝的アルゴリズムで特徴量選択](https://horomary.hatenablog.com/entry/2019/03/10/190919)