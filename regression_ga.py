# -*- coding: utf-8 -*-

from deap import algorithms
from deap import base
from deap import creator
from deap import tools
from sklearn.linear_model import LinearRegression
import numpy as np
from sklearn.model_selection import train_test_split
import sys

class GA_Selector:
    def __init__(self, x, y, test_size=0.2, MGEN=50, MU=100, LAMBDA=300, CXPB=0.7, MUTPB=0.1):
        """遺伝的アルゴリズム設定
        50世代まで
        1世代の個体数300
        次世代に引き継ぐ個体数100
        交叉確率0.7
        突然変異確率0.1
        """
        self.MGEN = MGEN
        self.MU = MU
        self.LAMBDA = LAMBDA
        self.CXPB = CXPB
        self.MUTPB = MUTPB
        self.test_size=test_size
        self.X = x
        self.y = y
 
        # 30回の実行での平均スコアが最大・最小となる組み合わせを保持しておく‥
        self.best_avg_feature_set = None
        self.best_avg_score = -9999
        self.worst_avg_feature_set = None
        self.worst_avg_score = 9999

    def evalIndividual(self, individual):
        """ 個体評価のための関数
            Prameter:
            individual: 要素が0or1のリスト  長さは入力変数の数と同じ
            Return:
            n_features: 使った特徴量の数
            score: モデルの精度
        """
        n_features = sum(individual)

        if n_features == 0:
            return 9999, -9999

        X_temp = self.X[:, [bool(val) for val in individual]]
        score_list = []

        # 学習データと検証データの分割を変えながら，30回学習を繰り返す
        for _ in range(30):
            X_train, X_test, y_train, y_test = train_test_split(X_temp, y, test_size=self.test_size)
            X_std_train, X_std_test = standardization(X_train, X_test)
            score, model = learn_and_eval(X_std_train, y_train, X_std_test, y_test)
            score_list = score_list + [score]

        avg_score = np.array(score_list).mean()

        # 平均のベストスコアを更新
        if avg_score > self.best_avg_score:
            self.best_avg_score = avg_score
            self.best_avg_feature_set = individual

        # 平均のワーストスコアを更新
        if avg_score < self.worst_avg_score:
            self.worst_avg_score = avg_score
            self.worst_avg_feature_set = individual
            
        return n_features, avg_score

    def select(self):
        """
        遺伝的アルゴリズムによる特徴量選択
        
        Returns:
            [type] -- [description]
        """
        creator.create("Fitness", base.Fitness, weights=(-1.0, 1.0))   
        creator.create("Individual", list, fitness=creator.Fitness)
        toolbox = base.Toolbox()
        toolbox.register("attr_bool", np.random.randint, 0, 1)
        toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_bool, self.X.shape[1])
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)
        toolbox.register("evaluate", self.evalIndividual)
        toolbox.register("mate", tools.cxTwoPoint)
        toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
        toolbox.register("select", tools.selNSGA2)
        pop = toolbox.population(n=self.MU)
        hof = tools.ParetoFront()
        stats = tools.Statistics(lambda ind: ind.fitness.values)
        stats.register("avg", np.mean, axis=0)
        stats.register("std", np.std, axis=0)
        stats.register("min", np.min, axis=0)
        stats.register("max", np.max, axis=0)

        return algorithms.eaMuPlusLambda(pop, toolbox, self.MU, self.LAMBDA, self.CXPB, self.MUTPB, self.MGEN, stats, halloffame=hof)


def standardization(train: np.ndarray, test: np.ndarray):
    """訓練データと検証データの標準化を行う関数
    Arguments:
        train {np.ndarray} -- [訓練データ]
        test {np.ndarray} -- [検証データ]
    Returns:
        Tule[np.ndarray, np.ndarray] -- [標準化後の訓練データと検証データ]
    """
    mean_on_train = train.mean(axis=0)
    std_on_train = test.std(axis=0)
    train_scaled = (train - mean_on_train) / std_on_train
    test_scaled = (test - mean_on_train) / std_on_train
    return train_scaled, test_scaled

def get_used_column_names_list(all_column_names_list, usage_list):
    result = []

    for i in range(len(usage_list)):
        if bool(usage_list[i]):
            result = result + [all_column_names_list[i]]

    return result

def learn_and_eval(X_std_train, y_train, X_std_test, y_test):
    model = LinearRegression()
    model.fit(X_std_train, y_train)
    score = model.score(X_std_test, y_test)

    return score, model

def read_column_name_list(file_name):
    """
    ファイルの先頭行から特徴量名のリストを取得する
    Returns:
    [List[str]] -- [列名のリスト]    
    """
    f = open(file_name)
    line = f.readline().strip()
    f.close()
    return line.split('\t')

if __name__ == "__main__":
    args = sys.argv
    if(len(args) != 3):
        print("Usage: python regression_ga.py [FILE] [MGEN]")
        exit()
    
    # 学習データの準備
    input_file_name = args[1]
    mgen = int(args[2])
    data = np.loadtxt(input_file_name, delimiter='\t', skiprows=1)
    column_names = read_column_name_list(input_file_name)
    y = data[:, 0]
    y_coulmn_name = column_names[0]
    x = data[:, 1:]
    x_column_name = column_names[1:]
    test_size = 0.2

    # 遺伝的アルゴリズムによる学習の開始
    gaselector = GA_Selector(x, y, MGEN=mgen, test_size=test_size)
    # select関数は最終世代の特徴の組み合わせを返すが，
    # 今回は使用しない．
    print("start best/worst feature selection with GA")
    gaselector.select()

    # 1/0で表現された特徴量の組み合わせを列名の組み合わせに変換する
    best_avg_feature_names = get_used_column_names_list(x_column_name, gaselector.best_avg_feature_set)
    worst_avg_feature_names = get_used_column_names_list(x_column_name, gaselector.worst_avg_feature_set)

    # 平均scoreがベストな特徴量の組み合わせ・全特徴量・平均scoreがワーストな特徴量の組み合わせでそれぞれ、学習させる。
    # こちらも30回，分割を変えがなら平均・最大・最小を取る
    print("start evaluation of models using best/worst/all feature sets")
    all_score_list = []
    best_score_list = []
    worst_score_list = []
    for _ in range(30):
        X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=test_size)
        X_std_train, X_std_test = standardization(X_train, X_test)
        # 全特徴量を使用した学習
        all_score, _ = learn_and_eval(X_std_train, y_train, X_std_test, y_test)
        all_score_list = all_score_list + [all_score]
        
        # ベストな特徴量の組み合わせ
        X_std_train_temp = X_std_train[:, [bool(val) for val in  gaselector.best_avg_feature_set]]
        X_std_test_temp = X_std_test[:, [bool(val) for val in  gaselector.best_avg_feature_set]]
        best_score, _ = learn_and_eval(X_std_train_temp, y_train, X_std_test_temp, y_test)
        best_score_list = best_score_list + [best_score]

        # ワーストな特徴量の組み合わせ
        X_std_train_temp = X_std_train[:, [bool(val) for val in  gaselector.worst_avg_feature_set]]
        X_std_test_temp = X_std_test[:, [bool(val) for val in  gaselector.worst_avg_feature_set]]
        worst_score, _ = learn_and_eval(X_std_train_temp, y_train, X_std_test_temp, y_test)
        worst_score_list = worst_score_list + [worst_score]      

    # 統計量の計算を簡単にするため、numpy配列にしておく
    np_all_score_list = np.array(all_score_list)
    np_best_score_list = np.array(best_score_list)
    np_worst_score_list = np.array(worst_score_list)

    # 結果の出力
    print("---------------")
    print("[all]")
    print("all feature set score(avg):{}".format(np_all_score_list.mean()))
    print("all feature set score(max):{}".format(np_all_score_list.max()))
    print("all feature set score(min):{}".format(np_all_score_list.min()))
    print("")

    print("[best]")
    print("best feature set:{}".format(",".join(best_avg_feature_names)))
    print("best feature set score(avg):{}".format(np_best_score_list.mean()))
    print("best feature set score(max):{}".format(np_best_score_list.max()))
    print("best feature set score(min):{}".format(np_best_score_list.min()))
    print("")

    print("[worst]")
    print("worst feature set:{}".format(",".join(worst_avg_feature_names)))
    print("worst feature set score(avg):{}".format(np_worst_score_list.mean()))
    print("worst feature set score(max):{}".format(np_worst_score_list.max()))
    print("worst feature set score(min):{}".format(np_worst_score_list.min()))
    print("---------------")